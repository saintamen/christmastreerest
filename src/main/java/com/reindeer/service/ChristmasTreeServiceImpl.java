package com.reindeer.service;


import com.reindeer.model.ChristmasTreeLevel;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service("christmasService")
public class ChristmasTreeServiceImpl implements IChristmasTreeService {
    private ChristmasTreeLevel ledLevel = new ChristmasTreeLevel(0);

    static {
        Logger.getLogger(ChristmasTreeServiceImpl.class.getName())
                .log(Level.WARNING, "Creating christmas tree with level 0.");
    }

    @Override
    public ChristmasTreeLevel getLedLevel() {
        return ledLevel;
    }

    @Override
    public void setLedLevel(ChristmasTreeLevel ledLevel) {
        Logger.getLogger(ChristmasTreeServiceImpl.class.getName())
                .log(Level.WARNING, "Setting led level to: " + ledLevel);
        this.ledLevel = ledLevel;

        try (PrintWriter writer = new PrintWriter(new File("/tmp/ledController"))) {

            writer.println("LedLevel:" + ledLevel.getLedLevel());
            writer.flush();
            Logger.getLogger(ChristmasTreeServiceImpl.class.getName())
                    .log(Level.WARNING, "Has been written to file: " + ledLevel.getLedLevel());
        } catch (FileNotFoundException e) {
            Logger.getLogger(ChristmasTreeServiceImpl.class.getName())
                    .log(Level.SEVERE, "Error while writing to file " + e.getMessage());
            e.printStackTrace();
        }
        Logger.getLogger(ChristmasTreeServiceImpl.class.getName())
                .log(Level.WARNING, "Method has finished.");
    }
}

