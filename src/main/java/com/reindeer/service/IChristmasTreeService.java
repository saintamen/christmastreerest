package com.reindeer.service;


import com.reindeer.model.ChristmasTreeLevel;

public interface IChristmasTreeService {

    public ChristmasTreeLevel getLedLevel();
    public void setLedLevel(ChristmasTreeLevel ledLevel);
}
