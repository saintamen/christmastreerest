package com.reindeer.controller;

import com.reindeer.service.ChristmasTreeServiceImpl;
import com.reindeer.model.ChristmasTreeLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class ChristmasController {

	@RequestMapping("/")
	String home(ModelMap modal) {
		modal.addAttribute("title", "Dear Learner");
		modal.addAttribute("message", "Welcome to SpringBoot");
		return "hello";
	}

	@Autowired
	ChristmasTreeServiceImpl christmasService;

	@RequestMapping(value = "/getlevel/", method = RequestMethod.GET)
	public ResponseEntity<ChristmasTreeLevel> getChristmasTreeLevel() {
		Logger.getLogger(ChristmasTreeServiceImpl.class.getName())
				.log(Level.WARNING, "Requesting christmas tree level.");
		return new ResponseEntity<ChristmasTreeLevel>(christmasService.getLedLevel(), HttpStatus.OK);
	}

	@RequestMapping(value = "/setlevel/", method = RequestMethod.POST)
	public ResponseEntity<Void> setChristmasTreeLevel(@RequestBody ChristmasTreeLevel level) {
		Logger.getLogger(ChristmasTreeServiceImpl.class.getName())
				.log(Level.WARNING, "Request setting christmas tree level: " + level);

		christmasService.setLedLevel(level);

		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
