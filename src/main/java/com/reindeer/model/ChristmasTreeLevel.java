package com.reindeer.model;

public class ChristmasTreeLevel {
    private int ledLevel;

    public ChristmasTreeLevel() {
    }

    public ChristmasTreeLevel(int ledLevel) {
        this.ledLevel = ledLevel;
    }

    public int getLedLevel() {
        return ledLevel;
    }

    public void setLedLevel(int ledLevel) {
        this.ledLevel = ledLevel;
    }

    @Override
    public String toString() {
        return "ChristmasTreeLevel{" +
                "ledLevel=" + ledLevel +
                '}';
    }
}
